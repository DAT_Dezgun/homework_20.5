// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HomeWork_20_5GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_20_5_API AHomeWork_20_5GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
